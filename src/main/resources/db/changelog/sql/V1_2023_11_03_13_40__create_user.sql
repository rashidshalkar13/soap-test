CREATE TABLE if NOT EXISTS users
(
    id                          serial NOT NULL,
    first_name                  varchar(255),
    last_name                   varchar(255),
    phone_number                varchar(255) NOT NULL,
    email                       varchar(255) NOT NULL,
    date_of_birth               date,
    constraint users_pk
    primary key (id)
);

create unique index users_phone_number_uindex
    on users (phone_number);

create unique index users_email_uindex
    on users (email);

comment on column users.first_name is 'Имя';

comment on column users.last_name is 'Фамилия';

comment on column users.phone_number is 'Номер телефона';

comment on column users.email is 'Емейл';

comment on column users.date_of_birth is 'Дата рождения';