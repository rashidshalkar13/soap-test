package kz.test.soapui.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;


import java.util.Date;

@Data
public class UserDto {
    private Long id;

    @NotNull(message = "firstName cannot be empty")
    @Size(min = 1, max = 255, message
            = "firstName must be between 1 and 255 characters")
    private String firstName;

    @NotNull(message = "lastName cannot be empty")
    @Size(min = 1, max = 255, message
            = "lastName must be between 1 and 255 characters")
    private String lastName;

    @NotNull(message = "Email cannot be empty")
    @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Email should be valid")
    private String email;

    @NotNull(message = "Phone cannot be empty")
    @Pattern(regexp = "^[\\+]7[0-9]{10}$", message = "Phone should be valid")
    private String phoneNumber;

    private Date dateOfBirth;
}
