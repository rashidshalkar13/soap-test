package kz.test.soapui.dto;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {
    List<T> data;
    int totalCount;

    public PageResult(List<T> data, int totalCount) {
        this.data = data;
        this.totalCount = totalCount;
    }
}
