package kz.test.soapui.controller;


import kz.test.soapui.controller.api.ApiErrorResponse;
import kz.test.soapui.controller.api.ApiEmptyResponse;
import kz.test.soapui.controller.api.ApiResponse;
import kz.test.soapui.dto.PageResult;
import kz.test.soapui.dto.UserDto;
import kz.test.soapui.service.UserService;
import kz.test.soapui.util.ValidationUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/api/user/", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("list")
    public ResponseEntity<PageResult<UserDto>> findUsers(@RequestParam(value = "page", defaultValue = "0") int page,
                                                        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return ResponseEntity.ok(userService.findUsers(pageSize, page));
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> findUser(@PathVariable(value = "id") Long id) throws Exception {
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @PostMapping("add")
    public ResponseEntity<ApiResponse> addUser(@RequestBody UserDto userDto) {
        try {
            List<String> errors = ValidationUtils.validate(userDto);
            if (!CollectionUtils.isEmpty(errors)) {
                return ResponseEntity.badRequest().body(ApiErrorResponse.validationError(errors));
            }

            userService.addUser(userDto);
            return ResponseEntity.ok(ApiEmptyResponse.create());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(ApiErrorResponse.create(e.getMessage()));
        }
    }

    @PostMapping("edit")
    public ResponseEntity<ApiResponse> editUser(@RequestBody UserDto userDto) {
        try {
            List<String> errors = ValidationUtils.validate(userDto);
            if (!CollectionUtils.isEmpty(errors)) {
                return ResponseEntity.badRequest().body(ApiErrorResponse.validationError(errors));
            }

            userService.edit(userDto);
            return ResponseEntity.ok(ApiEmptyResponse.create());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(ApiErrorResponse.create(e.getMessage()));
        }
    }

    @DeleteMapping("delete")
    public ResponseEntity<ApiResponse> deleteUser(@RequestParam("id") Long id) {
        try {
            userService.delete(id);
            return ResponseEntity.ok(ApiEmptyResponse.create());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(ApiErrorResponse.create(e.getMessage()));
        }
    }
}
