package kz.test.soapui.controller;

import kz.test.soapui.dto.UserDto;
import kz.test.soapui.service.UserService;
import kz.test.soapui.errors.CustomException;
import kz.test.soapui.service.mapper.UserMapper;
import kz.test.soapui.util.ValidationUtils;
import localhost._8089.ws.AddUserRequest;
import localhost._8089.ws.GetUserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class UserEndpoint {

    private static final String NAMESPACE_URI = "http://localhost:8089/ws";

    private UserService userService;

    @Autowired
    public UserEndpoint(UserService userService) {
        this.userService = userService;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addUserRequest")
    @ResponsePayload
    public GetUserResponse addUser(@RequestPayload AddUserRequest request) throws Exception {
        GetUserResponse response = new GetUserResponse();

        UserDto userDto = UserMapper.MAPPER.mapToDto(request);

        List<String> errors = ValidationUtils.validate(userDto);

        if (!errors.isEmpty()) {
            throw new CustomException(errors);
        }

        UserDto dto = userService.addUser(userDto);

        response.setUser(UserMapper.MAPPER.mapToXmlUser(dto));

        return response;
    }
}
