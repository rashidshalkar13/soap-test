package kz.test.soapui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapuiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapuiApplication.class, args);
	}

}
