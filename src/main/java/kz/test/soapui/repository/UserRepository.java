package kz.test.soapui.repository;

import kz.test.soapui.model.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UsersEntity, Long> {

    Optional<UsersEntity> findByPhoneNumber(String phoneNumber);

    Optional<UsersEntity> findByEmail(String email);
}
