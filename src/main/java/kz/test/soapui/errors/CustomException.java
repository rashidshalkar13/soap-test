package kz.test.soapui.errors;

import java.util.List;

public class CustomException extends RuntimeException {

    private List<String> errors;

    public CustomException(List<String> s) {
        this.errors = s;
    }

    public List<String> getMyStrings() {
        return errors;
    }

    public String getMessage() {
        return String.join(", ", errors);
    }
}
