package kz.test.soapui.service;

import jakarta.validation.constraints.NotNull;
import kz.test.soapui.dto.PageResult;
import kz.test.soapui.dto.UserDto;
import kz.test.soapui.model.UsersEntity;
import kz.test.soapui.repository.UserRepository;
import kz.test.soapui.service.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public void edit(@NotNull UserDto userDto) throws Exception {
        UsersEntity user = userRepository.findById(userDto.getId()).orElseThrow(() -> new Exception("Пользователь не найден"));
        if (!user.getPhoneNumber().equals(userDto.getPhoneNumber())) {
            Optional<UsersEntity> duplicate = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
            if (duplicate.isPresent()) {
                throw new Exception("Пользователь с данным номером телефона уже зарегистрирован в системе");
            }
        }

        if (StringUtils.hasText(userDto.getEmail()) && !userDto.getEmail().equals(user.getEmail())) {
            Optional<UsersEntity> duplicate = userRepository.findByEmail(userDto.getEmail());
            if (duplicate.isPresent()) {
                throw new Exception("Пользователь с данным электронным адресом уже зарегистрирован в системе");
            }
        }
        user = UserMapper.MAPPER.mapToEntity(userDto);
        userRepository.save(user);
    }

    public PageResult<UserDto> findUsers(int pageSize, int page) {
        Page<UsersEntity> pagePart = userRepository.findAll(PageRequest.of(page, pageSize));
        List<UserDto> list = pagePart.getContent().stream().map(UserMapper.MAPPER::mapToDto).collect(Collectors.toList());
        return new PageResult<>(list, (int) pagePart.getTotalElements());
    }

    @Override
    public void delete(Long id) throws Exception {
        Optional<UsersEntity> optionalUser = userRepository.findById(id);
        UsersEntity usersEntity = optionalUser.orElseThrow(() -> new Exception("Пользователь не найден"));

        userRepository.delete(usersEntity);
    }

    @Override
    public UserDto findUserById(Long id) throws Exception {
        UsersEntity usersEntity = userRepository.findById(id).orElseThrow(() -> new Exception("Пользователь не найден"));
        return UserMapper.MAPPER.mapToDto(usersEntity);
    }

    public UserDto addUser(@NotNull UserDto userDto) throws Exception {

        Optional<UsersEntity> duplicate = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
        if (duplicate.isPresent()) {
            throw new Exception("Пользователь с данным номером телефона уже зарегистрирован в системе");
        }

        duplicate = userRepository.findByPhoneNumber(userDto.getEmail());
        if (duplicate.isPresent()) {
            throw new Exception("Пользователь с данным электронным адресом уже зарегистрирован в системе");
        }

        userDto.setId(null);
        UsersEntity user = UserMapper.MAPPER.mapToEntity(userDto);
        userRepository.save(user);

        return UserMapper.MAPPER.mapToDto(user);
    }
}
