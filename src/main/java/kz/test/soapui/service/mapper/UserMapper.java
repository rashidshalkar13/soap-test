package kz.test.soapui.service.mapper;

import kz.test.soapui.dto.UserDto;
import kz.test.soapui.model.UsersEntity;
import localhost._8089.ws.AddUserRequest;
import localhost._8089.ws.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Mapper
public interface UserMapper {
    UserMapper MAPPER = Mappers.getMapper(UserMapper.class);

    UserDto mapToDto(UsersEntity usersEntity);

    UsersEntity mapToEntity(UserDto userDto);
    @Mapping(target = "dateOfBirth", expression = "java(toDate(request.getDateOfBirth()))")
    UserDto mapToDto(AddUserRequest request);

    @Mapping(target = "dateOfBirth", expression = "java(toXMLGregorianCalendar(userDto.getDateOfBirth()))")
    User mapToXmlUser(UserDto userDto);

    default XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar
                    = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            System.out.println(ex);
        }
        return xmlCalendar;
    }

    default Date toDate(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
}
