package kz.test.soapui.service;

import jakarta.validation.constraints.NotNull;
import kz.test.soapui.dto.PageResult;
import kz.test.soapui.dto.UserDto;

public interface UserService {

    void edit(@NotNull UserDto userDto) throws Exception;

    UserDto addUser(UserDto userDto) throws Exception;

    PageResult<UserDto> findUsers(int pageSize, int page);

    void delete(Long id) throws Exception;

    UserDto findUserById(Long id) throws Exception;
}
